<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LookupRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use Knuckles\Scribe\Attributes\Group;
use Knuckles\Scribe\Attributes\UrlParam;
use Request;
use Symfony\Component\HttpFoundation\Response;
use WessamA\BinLookup\Database\SQLiteManager;
use WessamA\BinLookup\Model\Bank;
use WessamA\BinLookup\Service\BankLogoService;

#[Group('Lookup', "APIs to lookup bank info")]
class LookupController extends Controller
{
    /**
     * Get bank logo by BIN.
     */
    #[UrlParam('bin', description: 'Bank Identification Number')]
    public function getLogo(Request $request, string $bin): Response
    {
        try {
            $bank = $this->fetchBankInfo($bin);

            $logoBinaryImage = $bank?->getLogo();

            if ($logoBinaryImage) {
                $filename = "images/{$bank->getTitle()}_{$bin}.jpeg";

                $responseBody = $bank->toArray();

                unset($responseBody['logo']);

                // Store the image
                Storage::disk('public')->put($filename, $logoBinaryImage);

                // Get the publicly accessible URL
                $url = Storage::disk('public')->url($filename);

                return response()->json(array_merge($responseBody, ['logo_url' => $url]));
            }

            return response()->json(['message' => 'No logo found'], Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error fetching logo', 'error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function getBankInfo(LookupRequest $request): JsonResponse
    {
        $validatedData = $request->validated();

        try {
            $bank = $this->fetchBankInfo($validatedData['bin']);

            if ($bank) {
                return response()->json($bank->toArray());
            }

            return response()->json(['message' => 'Not found'], Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error fetching bank info', 'error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @throws Exception
     */
    private function fetchBankInfo(string $bin): ?Bank
    {
        $config = config('binlookup');

        $dbManager = new SQLiteManager($config);

        $logoService = new BankLogoService($dbManager,$config);

        return $logoService->fetchAndSaveLogo($bin);
    }
}
